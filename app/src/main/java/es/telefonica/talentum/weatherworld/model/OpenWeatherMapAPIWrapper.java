package es.telefonica.talentum.weatherworld.model;


public class OpenWeatherMapAPIWrapper {

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";


    public String getCurrentWeatherCondition(City city) {
        String url = BASE_URL + city.getName()+ ",es&amp;lang=es";
        return url;
    }

}
