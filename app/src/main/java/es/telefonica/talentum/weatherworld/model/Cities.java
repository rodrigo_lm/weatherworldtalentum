package es.telefonica.talentum.weatherworld.model;


import java.util.ArrayList;
import java.util.List;

public class Cities {

    private List<City> cities;

    public Cities() {

    }

    // LAZY GETTER
    public List<City> getCities() {
        if (cities == null) {
            cities = new ArrayList<City>();
        }
        return cities;
    }

    public int size() {
        return getCities().size();
    }

    public void addCity(City city) {
        getCities().add(city);
    }

    public void deleteCityAtIndex(int idx) {
        getCities().remove(idx);
    }

    public List allCities() {
        return getCities();
    }

    public List<String> cityNames() {
        List<String> names = new ArrayList<String>();
        for (City city: getCities()) {
            names.add(city.getName());
        }
        return names;
    }

}
