package es.telefonica.talentum.weatherworld;


import android.app.Application;
import android.util.Log;

public class WeatherWorldApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(this.getClass().toString(), "starting app");

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        Log.wtf(this.getClass().toString(), "No memory");
    }
}
