package es.telefonica.talentum.weatherworld.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import es.telefonica.talentum.weatherworld.Constants;
import es.telefonica.talentum.weatherworld.R;
import es.telefonica.talentum.weatherworld.model.City;
import es.telefonica.talentum.weatherworld.model.OpenWeatherMapAPIWrapper;
import es.telefonica.talentum.weatherworld.model.OpenWeatherMapJSONParser;
import es.telefonica.talentum.weatherworld.model.WeatherCondition;
import es.telefonica.talentum.weatherworld.net.JSONUtil;

public class WeatherForecastActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_forecast);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather_forecast, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        City city;

        ImageView weatherIcon;
        TextView cityName;
        TextView temperature;
        ProgressBar activityIndicator;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_weather_forecast, container, false);

            weatherIcon = (ImageView) rootView.findViewById(R.id.weatherIcon);
            cityName = (TextView) rootView.findViewById(R.id.city_name);
            temperature = (TextView) rootView.findViewById(R.id.temperature);
            activityIndicator = (ProgressBar) rootView.findViewById(R.id.progressBar);

            Intent i = getActivity().getIntent();
            city = i.getParcelableExtra(Constants.KEY_INTENT_SHOW_CITY_CURRENT_CONDITION);

            getWeather(city);

            return rootView;
        }

        private void getWeather(City city) {
            // 1 montar la petición

            OpenWeatherMapAPIWrapper api = new OpenWeatherMapAPIWrapper();
            String url = api.getCurrentWeatherCondition(city);

            // 2 ejecutar la petición

            // OFFENDING TASK
            WeatherDownloadTask task = new WeatherDownloadTask();
            task.execute(url);

            // 3 parsear el JSON
            // TODO:

            Log.d("JSON", "JSON con la boina a rosca ");

        }

        private class WeatherDownloadTask extends AsyncTask<String, Integer, JSONObject> {
            @Override
            protected void onPreExecute() { // MAIN, PUEDES CAMBIAR UI
                super.onPreExecute();

                activityIndicator.setVisibility(View.VISIBLE);
            }

            /********** POR TU MADRE NO TOQUES UI AQUI ************/
            @Override
            protected JSONObject doInBackground(String... params) {

                JSONObject json = JSONUtil.getJSONFromHttpRequest(params[0]);

                return json;
            }
            /********** POR TU MADRE NO TOQUES UI AQUI ************/


            @Override
            protected void onProgressUpdate(Integer... values) { // MAIN, PUEDES CAMBIAR UI
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(JSONObject json) { // MAIN, PUEDES CAMBIAR UI
                super.onPostExecute(json);
                activityIndicator.setVisibility(View.GONE);

                OpenWeatherMapJSONParser parser = new OpenWeatherMapJSONParser();
                WeatherCondition condition = parser.parseWeatherCondition(json);

                temperature.setText(condition.getTemperature());
                cityName.setText(condition.getDescription());
            }
        }



    }
}
