package es.telefonica.talentum.weatherworld.model;

public class MockDataBase {

    public Cities query() {
        Cities cities = new Cities();

        City c = new City("Madrid");
        cities.addCity(c);

        for (int i=0; i<1; i++) {
            City city = new City("My City " + i);

            cities.addCity(city);
        }

        return cities;
    }

}
