package es.telefonica.talentum.weatherworld;

public class Constants {
    public static final String KEY_INTENT_EDIT_CITY = "es.telefonica.talentum.weatherworld.KEY_INTENT_EDIT_CITY";

    public static final int ADDING_CITY = 10;
    public static final String KEY_RESULT_INTENT_ADDED_CITY = "es.telefonica.talentum.weatherworld.KEY_RESULT_INTENT_ADDED_CITY";
    public static final String KEY_INTENT_SHOW_CITY_CURRENT_CONDITION = "es.telefonica.talentum.weatherworld.KEY_INTENT_SHOW_CITY_CURRENT_CONDITION";


}
