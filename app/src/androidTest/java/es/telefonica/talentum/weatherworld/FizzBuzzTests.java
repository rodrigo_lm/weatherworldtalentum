package es.telefonica.talentum.weatherworld;


import android.test.AndroidTestCase;

import es.telefonica.talentum.weatherworld.model.FizzBuzz;

public class FizzBuzzTests extends AndroidTestCase {

    public void testFizzBuzzReturns1With1() {
        String returnValue = FizzBuzz.fizzBuzz(1);

        assertEquals("1", returnValue);
    }

    public void testFizzBuzzReturnsFizzWith3() {
        String returnValue = FizzBuzz.fizzBuzz(3);

        assertEquals("Fizz", returnValue);
    }

    public void testFizzBuzzReturnsFizzWith6() {
        String returnValue = FizzBuzz.fizzBuzz(6);

        assertEquals("Fizz", returnValue);
    }

    public void testFizzBuzzReturnsBuzzWith5() {
        String returnValue = FizzBuzz.fizzBuzz(5);

        assertEquals("Buzz", returnValue);
    }

    public void testFizzBuzzReturnsFizzBuzzWith15() {
        String returnValue = FizzBuzz.fizzBuzz(15);

        assertEquals("FizzBuzz", returnValue);
    }
}
